package fmj.combat.actions

import fmj.characters.FightingCharacter
import fmj.characters.Monster
import fmj.combat.anim.RaiseAnimation
import fmj.goods.BaseGoods
import graphics.Canvas

abstract class ActionSingleTarget(attacker: FightingCharacter,
                                  protected var mTarget: FightingCharacter) : Action() {

    protected var mRaiseAni: RaiseAnimation? = null

    override val isTargetAlive: Boolean
        get() = mTarget.isAlive

    override val isSingleTarget: Boolean
        get() = false

    init {
        mAttacker = attacker
    }

    override fun postExecute() {
        mTarget.isVisiable = mTarget.isAlive
    }

    override fun updateRaiseAnimation(delta: Long): Boolean {
        return mRaiseAni?.update(delta) ?: false
    }

    override fun drawRaiseAnimation(canvas: Canvas) {
        mRaiseAni?.draw(canvas)
    }

    override fun targetIsMonster(): Boolean {
        return mTarget is Monster
    }

    fun setTarget(fc: FightingCharacter) {
        mTarget = fc
    }

    fun steal(attacker: FightingCharacter): BaseGoods? {
        val target = mTarget
        if (target is Monster) {
            return target.stealGoods(attacker)
        }
        return null
    }
}
