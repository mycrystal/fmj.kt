package fmj.goods

import fmj.characters.Player

interface IEatMedicine {
    fun eat(player: Player)
}
